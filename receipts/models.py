from string import digits
from unittest.util import _MAX_LENGTH
from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="categories"
    )

    def __str__(self):
        return self.name


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="accounts"
    )

    def __str__(self):
        return self.name


class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(decimal_places=3, max_digits=10)
    tax = models.DecimalField(decimal_places=3, max_digits=10)
    date = models.DateTimeField(auto_now=True)
    purchaser = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="receipts"
    )
    category = models.ForeignKey(
        ExpenseCategory, related_name="receipts", on_delete=models.CASCADE
    )
    account = models.ForeignKey(
        Account, related_name="receipts", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.vendor + " : " + str(self.date)
