from django.urls import path
from receipts.views import my_receipts

urlpatterns = [
    path("", my_receipts, name="home"),
]
