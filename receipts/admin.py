from django.contrib import admin

# Register your models here.
from receipts.models import ExpenseCategory
from receipts.models import Account
from receipts.models import Receipt


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Account)
class Account(admin.ModelAdmin):
    pass


@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    pass
