from django.shortcuts import render
from .models import Receipt

# Create your views here.
def my_receipts(request):
    my_receipts = Receipt.objects.all()
    context = {
        "receipts": my_receipts,
    }
    return render(request, "receipts/myreceipts.html", context)
